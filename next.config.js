module.exports = {
  distDir: 'build',
  publicRuntimeConfig: {
    backendUrl: process.env.SERVICE_URL,
  },
};
