import { awaitStatus } from 'utils/sockets';
import * as TYPE from 'actions';

export const awaitUserData = ({ socket, identifier }) => async dispatch => {
  try {
    const response = await awaitStatus({ socket, identifier });
    dispatch({
      type: TYPE.USER_SET_PROFILE,
      payload: response,
    });
  } catch (error) {
    throw error;
  }
};
