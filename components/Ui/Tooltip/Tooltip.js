const Tooltip = ({ triggerElement, children }) => {
  return (
    <div className="Tooltip">
      <aside className="Tooltip__Content a-tooltip a-tooltip--top">
        {children}
      </aside>
      {triggerElement}

      <style jsx>{`
        .Tooltip {
          position: relative;
        }
        .Tooltip__Content {
          position: absolute;
          bottom: ${triggerElement ? '35px' : 'initial'};
          display: ${triggerElement ? 'none' : 'initial'};
          transform: translateX(-50%);
        }
        .Tooltip:hover > .Tooltip__Content {
          display: initial;
        }
      `}</style>
    </div>
  );
};

export default Tooltip;
