import { withRouter } from 'next/router';
import { Tabs, Button } from '@acpaas-ui/react-components';

import { withNamespaces, Link } from 'i18n';

let DashboardLayout = ({ children, router, t }) => {
  const tabs = [
    {
      name: t('dashboard.tabs.id-card.title'),
      target: '/dashboard/id-card-credential',
    },
    // { name: t('dashboard.tabs.residence-permit.title'), target: '/dashboard/residence-permit' },
  ];

  return (
    <main>
      <header>
        <div className="u-container">
          <h3>{t('common.main-title')}</h3>
        </div>
      </header>

      <div className="u-container">
        <Tabs
          items={tabs.map(tab => ({
            ...tab,
            active: router.pathname === tab.target,
          }))}
          align="center"
          linkProps={props => ({
            ...props,
            component: ({ href, ...rest }) => (
              <Link href={href}>
                <Button outline type="primary" size="small" {...rest} />
              </Link>
            ),
          })}
        />

        <section>{children}</section>
      </div>

      <style jsx>{`
        header {
          overflow: auto;
          background-color: #f3f3f3;
          padding-bottom: 60px;
          padding-top: 40px;
          border-bottom: 1px solid #0064b4;
        }
        main :global(.m-nav-tabs) {
          margin-top: -3rem;
        }
        main :global(.m-nav-tabs button) {
          width: 14.38rem;
          line-height: 1rem;
          border: none;
          color: #0064b4;
          font-weight: 400;
        }
        main :global(.m-nav-tabs button.is-active),
        main :global(.m-nav-tabs button:hover) {
          border: none;
          background-color: #ffffff;
          font-weight: 700;
        }
        main :global(.m-nav-tabs li + li button) {
          border-left: none;
        }

        section {
          margin-top: 3.13rem;
        }

        @media (max-width: 45em) {
          main :global(.m-nav-tabs button) {
            width: 100%;
          }
        }
      `}</style>
    </main>
  );
};

DashboardLayout = withNamespaces()(DashboardLayout);
DashboardLayout = withRouter(DashboardLayout);

export default DashboardLayout;
