import { Field } from 'react-final-form';
import { required } from 'redux-form-validators';
import {
  Button,
  TextField,
  Datepicker,
  Select,
  RadioGroup,
} from '@acpaas-ui/react-components';

export const aKaartNumbers = [
  '900000255993',
  '900003070704',
  '900003070589',
  '900003070282',
  '900003070316',
  '900001426262',
  '900003070720',
  '900003070035',
  '900000256074',
];

const CredentialsForm = ({ countries, handleSubmit, pristine, invalid, t }) => {
  return (
    <div>
      <form
        onSubmit={handleSubmit}
        className="a-form u-margin-top-lg margin-center"
      >
        <Field
          validate={required()}
          name="familyName"
          label={t('dashboard.tabs.id-card.form.fields.familyName.name')}
          render={({ input, ...rest }) => <TextField {...input} {...rest} />}
        />
        <Field
          validate={required()}
          name="givenName"
          label={t('dashboard.tabs.id-card.form.fields.givenName.name')}
          render={({ input, ...rest }) => <TextField {...input} {...rest} />}
        />
        <Field
          autoClose
          validate={required()}
          name="birthDate"
          label={`${t('dashboard.tabs.id-card.form.fields.birthDate.name')}`}
          render={({ input, ...rest }) => (
            <Datepicker format="DD/MM/YYYY" {...input} {...rest} />
          )}
          open={false}
        />
        <Field
          validate={required()}
          name="birthPlace"
          label={t('dashboard.tabs.id-card.form.fields.birthPlace.name')}
          render={({ input, ...rest }) => <TextField {...input} {...rest} />}
        />
        <Field
          validate={required()}
          name="nationality"
          label={t('dashboard.tabs.id-card.form.fields.nationality.name')}
          options={[{ name: '' }, ...countries].map(country => ({
            label: country.name,
            value: country.name,
          }))}
          render={({ input, ...rest }) => <Select {...input} {...rest} />}
        />
        <Field
          validate={required()}
          className="u-margin-top-lg u-margin-bottom-lg"
          name="gender"
          label={t('dashboard.tabs.id-card.form.fields.gender.name')}
          options={[
            {
              value: 'female',
              label: t('common.form.fields.gender.options.female'),
            },
            {
              value: 'male',
              label: t('common.form.fields.gender.options.male'),
            },
          ]}
          render={({ input, ...rest }) => <RadioGroup {...input} {...rest} />}
        />
        <div className="row">
          <div className="col-xs-6">
            <Field
              autoClose
              validate={required()}
              name="validFrom"
              label={`${t(
                'dashboard.tabs.id-card.form.fields.validFrom.name'
              )}`}
              render={({ input, ...rest }) => (
                <Datepicker format="DD/MM/YYYY" {...input} {...rest} />
              )}
              open={false}
            />
          </div>
          <div className="col-xs-6">
            <Field
              autoClose
              validate={required()}
              name="validThrough"
              label={`${t(
                'dashboard.tabs.id-card.form.fields.validThrough.name'
              )}`}
              render={({ input, ...rest }) => (
                <Datepicker format="DD/MM/YYYY" {...input} {...rest} />
              )}
              open={false}
            />
          </div>
        </div>
        <Field
          validate={required()}
          name="documentNumber"
          label={t('dashboard.tabs.id-card.form.fields.documentNumber.name')}
          render={({ input, ...rest }) => <TextField {...input} {...rest} />}
        />

        <div className="row u-margin-top-lg">
          <div className="col-xs-12">
            <Field
              name="aKaartNumber"
              label={t('A-Kaart number')}
              options={["Don't issue an A-Kaart number", ...aKaartNumbers].map(
                number => ({
                  label: number,
                  value: number,
                })
              )}
              render={({ input, ...rest }) => <Select {...input} {...rest} />}
            />
          </div>
        </div>

        <div className="row u-margin-top-lg">
          <div className="col-xs-12 col-sm-6">
            <Button
              block
              type="success"
              disabled={pristine || invalid}
              className="col-xs col-xs-offset-1"
            >
              {t('common.button.submit')}
            </Button>
          </div>
        </div>
      </form>

      <style jsx>{`
        form {
          max-width: 460px;
        }

        form :global(.a-input) {
          width: 100%;
        }

        form :global(.a-input) {
          width: 100%;
        }
      `}</style>
    </div>
  );
};

export default CredentialsForm;
