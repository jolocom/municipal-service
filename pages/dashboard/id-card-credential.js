import React from 'react';
import { connect } from 'react-redux';
import { Form } from 'react-final-form';
import { withNamespaces } from 'i18n';
import { DashboardLayout, QrCodeItem, CredentialsForm } from 'components';
import { getQrCode, awaitStatus } from 'utils/sockets';
import { aKaartNumbers } from '../../components/Dashboard/CredentialsForm/CredentialsForm';
import Router from 'next/router';

class IdCardCredential extends React.Component {
  state = {
    credentialOffers: {
      id: {},
      aKart: {},
    },
    aKaartNumber: '',
  };

  handleSubmitForm = async({ aKaartNumber, ...userData }) => {
    const { credentialOffers } = this.state;

    try {
      // qrCode, socket, identifier
      const { qrCode: idCardQrCode, socket, identifier } = await getQrCode(
        'receive',
        {
          credentialType: 'id-card',
          data: JSON.stringify(userData),
        }
      );

      credentialOffers.id.source = idCardQrCode;
      this.setState({ credentialOffers, aKaartNumber });

      await awaitStatus({ socket, identifier });
      credentialOffers.id.success = true;

      this.setState({ credentialOffers });

      if (aKaartNumbers.indexOf(aKaartNumber) > -1) {
        const { qrCode: aKaartQrCode, socket, identifier } = await getQrCode(
          'receive',
          {
            credentialType: 'a-kaart',
            data: JSON.stringify({
              identifier: aKaartNumber,
            }),
          }
        );

        credentialOffers.aKart.source = aKaartQrCode;
        this.setState({ credentialOffers });

        await awaitStatus({ socket, identifier });

        credentialOffers.aKart.success = true;
        this.setState({ credentialOffers });
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { t } = this.props;
    const { credentialOffers, aKaartNumber } = this.state;

    if (!this.props.profile.did) {
      Router.push('/');
      return {};
    }

    return (
      <DashboardLayout>
        <div className="u-container">
          {credentialOffers.id.source || credentialOffers.aKart.source ? (
            <div className="row center-xs">
              <div className="col-xs-12 col-xs-6">
                <QrCodeItem
                  note={`${aKaartNumber ? '1. ' : ''}${t(
                    'dashboard.tabs.id-card.instructions.id-credential'
                  )}:`}
                  {...credentialOffers.id}
                />
              </div>
              {aKaartNumber && (
                <div className="col-xs-12 col-xs-6">
                  <QrCodeItem
                    note={`2. ${t(
                      'dashboard.tabs.id-card.instructions.a-kart'
                    )}:`}
                    {...credentialOffers.aKart}
                  />
                </div>
              )}
            </div>
          ) : (
            <div style={{ marginBottom: 120 }}>
              <Form
                onSubmit={this.handleSubmitForm}
                component={CredentialsForm}
                countries={this.props.countries}
                t={t}
              />
            </div>
          )}
        </div>
      </DashboardLayout>
    );
  }
}

IdCardCredential = withNamespaces()(IdCardCredential);

IdCardCredential = connect(
  ({ user: { profile }, ui: { globalError, countries } }) => ({
    profile,
    globalError,
    countries,
  })
)(IdCardCredential);

export default IdCardCredential;
