import Document, { Head, Main, NextScript } from 'next/document';

export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <html>
        <Head>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />
          <link
            rel="stylesheet"
            href="https://cdn.antwerpen.be/core_flexboxgrid_scss/1.0.1/flexboxgrid.min.css"
          />
          <link
            rel="stylesheet"
            href="https://cdn.antwerpen.be/core_branding_scss/3.0.3/main.min.css"
          />
          <link
            rel="stylesheet"
            href="https://cdn.antwerpen.be/core_branding_scss/3.0.3/antwerpen.min.css"
          />
          <link
            rel="stylesheet"
            href="https://cdn.antwerpen.be/core_branding_scss/3.0.3/sun-antwerpen.min.css"
          />
          <title>Antwerp municipal service</title>

          <style>{`
            html {
              color: #002135;
              font-size: 16px;
              font-family: Sun Antwerpen;
            }
            body {
              height: 100%;
            }
            * {
              box-sizing: border-box;
            }
            h1,h2,h3 {
              margin: 2.5rem 0;
              text-align: center;
              white-space: pre-wrap;
            }
            p {
              color: #002239;
              margin: 1rem 0;
              white-space: pre-wrap;
            }
            .text-gray {
              color: #B0B0B0;
            }
            .margin-center {
              margin: auto;
            }
            .u-full {
              width: 100%;
            }

            .a-input label:first-letter,
            .a-button:first-letter {
              text-transform: capitalize;
            }
          `}</style>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
