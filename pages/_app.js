import withRedux from 'next-redux-wrapper';
import { Provider } from 'react-redux';
import fetch from 'node-fetch';
import App, { Container } from 'next/app';

import { appWithTranslation, i18n } from 'i18n';
import { LanguageSwitcher } from 'components';
import { setCountries } from 'actions/ui';
import initStore from 'utils/store';

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};
    try {
      if (Component.getInitialProps) {
        pageProps = await Component.getInitialProps(ctx);
      }

      if (ctx.store.getState().ui.countries.length === 0) {
        const response = await fetch(
          'https://restcountries.eu/rest/v2/all/?fields=name;alpha2Code;flag'
        );
        pageProps.countries = await response.json();

        ctx.store.dispatch(setCountries(pageProps.countries));
      }

      return { pageProps };
    } catch (error) {
      console.log(error);

      return { pageProps };
    }
  }

  render() {
    const { Component, pageProps, store } = this.props;

    return (
      <Container>
        <Provider store={store}>
          <aside>
            <div className="u-container row between-xs">
              <figure className="m-image">
                <img src="/static/images/Bitmap.png" alt="figure image" />
              </figure>

              <LanguageSwitcher i18n={i18n} />
            </div>
          </aside>

          <Component {...pageProps} />

          <style jsx>{`
            aside {
              position: absolute;
              top: 0;
              width: 100%;
              z-index: 10;
            }
            figure {
              width: 2.5rem;
              height: 2.5rem;
              z-index: 1;
            }
          `}</style>
        </Provider>
      </Container>
    );
  }
}

MyApp = appWithTranslation(MyApp);
MyApp = withRedux(initStore)(MyApp);

export default MyApp;
