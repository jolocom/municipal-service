import { connect } from 'react-redux';
import Router from 'next/router';
import { Button, Alert } from '@acpaas-ui/react-components';

import { withNamespaces } from 'i18n';
import { Tooltip } from 'components';

import { awaitUserData } from 'actions/auth';

import { getQrCode } from 'utils/sockets';

class Index extends React.Component {
  state = {
    qrCode: {},
  };

  handleSSO = async() => {
    const { qrCode } = this.state;

    try {
      const { qrCode: ssoQrCode, socket, identifier } = await getQrCode(
        'authenticate'
      );

      qrCode.source = ssoQrCode;

      this.setState({ qrCode });

      await this.props.awaitUserData({ socket, identifier });

      Router.push('/dashboard/id-card-credential');
    } catch (error) {
      qrCode.error = error.data;
      this.setState({ qrCode });
    }
  };

  render() {
    const { t } = this.props;
    const { qrCode } = this.state;

    return (
      <div className="u-container u-text-center u-margin-bottom">
        {!qrCode.source && (
          <figure className="m-image">
            <img
              src="/static/images/Landing-image.png"
              width="884"
              alt="figure image"
            />
          </figure>
        )}
        <header>
          <h1>{t('common.main-title')}</h1>
          <p>
            {qrCode.source
              ? t('signIn.qr-code-scan-instruction')
              : t('signIn.main-description')}
          </p>
        </header>

        {qrCode.source ? (
          <>
            {qrCode.error && (
              <Alert
                closable
                type="danger"
                title={`${t('common.general-error')}!`}
                className="margin-center"
              >
                {t('signIn.error.403')}
              </Alert>
            )}
            <figure className="m-image">
              <img src={qrCode.source} alt="figure image" />
            </figure>

            <Tooltip
              triggerElement={
                <p className="text-gray">
                  {t('signIn.tooltip.does-not-work.title')}?
                </p>
              }
            >
              {t('signIn.tooltip.does-not-work.content')}.
            </Tooltip>
          </>
        ) : (
          <Button
            type="secondary"
            size="small"
            className="u-margin-top"
            onClick={this.handleSSO}
          >
            {t('common.button.log-in')}
          </Button>
        )}

        <style jsx>{`
          :global(html) {
            background-color: ${qrCode.source ? '#FFFFFF' : '#F4F3F4'};
            transition: background-color 1500ms linear;
          }

          header h1 {
            max-width: 450px;
            margin-left: auto;
            margin-right: auto;
            padding-top: 40px;
          }
        `}</style>
      </div>
    );
  }
}

Index = connect(
  ({ ui: { globalError } }) => ({
    globalError,
  }),
  {
    awaitUserData,
  }
)(Index);

Index = withNamespaces()(Index);

export default Index;
